<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Goutte;

class ScraperController extends Controller
{
    private $results    =   array();

    public static function scraper()
    {
        
        $client     =   new Client();
        $url        =   'http://www.bcv.org.ve/';
        $crawler    =  Goutte::request('GET', $url);
        $euro       =   $crawler->filter('#euro')->each(function ($node){ return    $node->text(); });
        $yuan       =   $crawler->filter('#yuan')->each(function ($node){ return    $node->text(); });
        $lira       =   $crawler->filter('#lira')->each(function ($node){ return    $node->text(); });
        $rublo      =   $crawler->filter('#rublo')->each(function ($node){ return    $node->text(); });
        $dolar      =   $crawler->filter('#dolar')->each(function ($node){ return    $node->text(); });
        
        $euro   =   str_replace(',','.',substr($euro[0],'4',strlen($euro[0])));
        $yuan   =   str_replace(',','.',substr($yuan[0],'4',strlen($yuan[0])));
        $lira   =   str_replace(',','.',substr($lira[0],'4',strlen($lira[0])));
        $rublo  =   str_replace(',','.',substr($rublo[0],'4',strlen($rublo[0])));
        $dolar  =   str_replace(',','.',substr($dolar[0],'4',strlen($dolar[0])));
        
        dd($euro, $yuan, $lira, $rublo, $dolar);
        
    }
}
